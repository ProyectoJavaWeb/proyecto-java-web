<%-- 
    Document   : Usuario
    Created on : 6/08/2015, 06:28:59 AM
    Author     : PC_16
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Registro Usuarios </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Tutoring Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="Resource/css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="Resource/css/style.css" rel='stylesheet' type='text/css' />
<script src="Resource/js/jquery.min.js"></script>
<script src="Resource/js/bootstrap.js"></script>
<!---- start-smoth-scrolling---->
<script type="text/javascript" src="Resource/js/move-top.js"></script>
<script type="text/javascript" src="Resource/js/easing.js"></script>
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
<!--start-smoth-scrolling-->
</head>
<body>

     
	<!--start-header-->
	<div class="header" id="home">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
                                <!--Cambiar logo  -->
				<a class="navbar-brand" href="index.html"><img src="Resource/images/logo.png" alt="" /></a>
                                
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				
				<div class="clearfix"></div>
			</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
	</div>
	<!--end-header-->
	<!--start-contact-->
	
	<!--end-contact-->
	<!--start-contact-->
        <br><br>
	<div class="contact">
		<div class="container">
			<div class="contact-top heading">
                            <h2>Registro de Usuarios</h2>
                        </div>
			<div class="contact-bottom"> 
				<div class="col-md-6 contact-left"> 
                                    
					<form id="addPersona" name="addUsuario" action="Usuarios?a=saveUsuarios" method="POST">
                                                
						<input name="Documento" type="text" placeholder="Documento" required="">
                                                
						<input name="Nombres" type="text" placeholder="Nombres"   required="">
                                                
						<input name="Apellidos" type="text" placeholder="Apellidos" required="">
                                                
                                                <input name="Direccion" type="text" placeholder="Direccion" required="">
                                                
                                                <input name="Telefono" type="text" placeholder="Telefono"  required="">
                                                
                                                <input name="Email" type="text" placeholder="Email" required="">
                                               
                                                <br>
                                                <input name="Contrasena" type="text" placeholder="Contraseña" required="">
                                                <br>
                                                
                                                <input name="TipoUsuario" type="text" placeholder="Tipo Usuario" required="">
                                                
                                                
                                                
                                                <input type="submit" value="Registrar" id="sendForm"></button>
					</form>
				</div>
				<div class="col-md-6 contact-left">
						
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--end-contact-->
	<!--start-footer-->
	<div class="footer">
		<div class="container">
			<div class="footer-main">
				<div class="col-md-4 footer-left">
					<span class="glyphicon glyphicon-map-marker map-marker" aria-hidden="true"></span>
					<p>The company name, <span>Lorem ipsum dolor,</span> Glasglow Dr 40 Fe 72.</p>
				</div>
				<div class="col-md-4 footer-left">
					<span class="glyphicon glyphicon-phone map-marker" aria-hidden="true"></span>
					<p>+1 800 603 6035 <span>+1 900 300 1320</span> </p>
				</div>
				<div class="col-md-4 footer-left">
					<p class="footer-class">© 2015 Tutoring All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
					<ul>
						<li><a href="#"><span class="fb"></span></a></li>
						<li><a href="#"><span class="twit"></span></a></li>
						<li><a href="#"><span class="rss"></span></a></li>
						<li><a href="#"><span class="ggl"></span></a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										$().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	</div>
	<!--end-footer-->
</body>
</html>
