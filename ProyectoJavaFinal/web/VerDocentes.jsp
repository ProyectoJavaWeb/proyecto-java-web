<%-- 
    Document   : VerDocentes
    Created on : 10/08/2015, 08:40:36 AM
    Author     : usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<title>Registro - Cursos</title>
<%
        if(session.getAttribute("usuario") ==null){
            response.sendRedirect("Login.jsp");
        }
        %>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Tutoring Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="Resource/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="Resource/css/style.css" rel='stylesheet' type='text/css' />
<script src="Resource/js/jquery.min.js"></script>
<script src="Resource/js/bootstrap.js"></script>
<!---- start-smoth-scrolling---->
<script src="Resource/js/move-top.js" type="text/javascript"></script>
<script type="text/javascript" src="Resource/js/easing.js"></script>
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
<!--start-smoth-scrolling-->
</head>
<body>
	<!--start-header-->
	<div class="header" id="home">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.html"><img src="Resource/images/logo.png" alt="" /></a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="active"><a href="IndexAdministrador.jsp" class="hvr-bounce-to-top">Inicio<span class="sr-only">(current)</span></a></li> 
					<li class="active"><a href="Docente.jsp" class="hvr-bounce-to-top">Registro Docentes<span class="sr-only">(current)</span></a></li>
					<li><a href="VerDocentes.jsp" class="hvr-bounce-to-top">Mostar Docentes</a></li>
					<li><a href="Curso.jsp" class="hvr-bounce-to-top">Registro Cursos</a></li>
					<li><a href="VerCursos.jsp" class="hvr-bounce-to-top">Mostrar Cursos</a></li>
					<li><a href="contact.html" class="hvr-bounce-to-top">Realizar Pagos</a></li>
                                        <li><a href="contact.html" class="hvr-bounce-to-top">Asignar Docente</a></li>
				</ul>
                            
				<div class="clearfix"></div>
			</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
	</div>
	
        <br><br>
	<div class="contact">
		<div class="container">
			<div class="contact-top heading">
                            <h2>Registro de Docentes</h2>
                        </div>
			<div class="contact-bottom"> 
				<div class="col-md-6 contact-left"> 
                                    <table border="1px">
                                        <td>Documento</td>                
                                        <td>Nombre</td>
                                        <td>Apellido</td>
                                        <td>Email</td>
                                        <td>Profesion</td>
                                        <td>Descripcion Prefil Tecnico</td>
                                        <td></td>
                                    
                                    <!-- estudiante = array en estudiante -->
                                <c:forEach items="${docente}" var="doc">
                                    <tr>

                                        <td>${doc.documento}</td>
                                        <td>${doc.nombre}</td>
                                        <td>${doc.apellido}</td>
                                        <td>${doc.email}</td>
                                        <td>${doc.profesion}</td>
                                        <td>${doc.descripcionPerfil}</td>
                                        <td><a href="Estudiantes?a=update&i=${est.idEstudiante}">Editar</a></td>

                                    </tr>
                                </c:forEach>
                                </table>
                            </div>
				
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--end-contact-->
	<!--start-footer-->
	<div class="footer">
		<div class="container">
			<div class="footer-main">
				<div class="col-md-4 footer-left">
					<span class="glyphicon glyphicon-map-marker map-marker" aria-hidden="true"></span>
					<p>The company name, <span>Lorem ipsum dolor,</span> Glasglow Dr 40 Fe 72.</p>
				</div>
				<div class="col-md-4 footer-left">
					<span class="glyphicon glyphicon-phone map-marker" aria-hidden="true"></span>
					<p>+1 800 603 6035 <span>+1 900 300 1320</span> </p>
				</div>
				<div class="col-md-4 footer-left">
					<p class="footer-class">© 2015 Tutoring All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
					<ul>
						<li><a href="#"><span class="fb"></span></a></li>
						<li><a href="#"><span class="twit"></span></a></li>
						<li><a href="#"><span class="rss"></span></a></li>
						<li><a href="#"><span class="ggl"></span></a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										$().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	</div>
	<!--end-footer-->
</body>
</html>
