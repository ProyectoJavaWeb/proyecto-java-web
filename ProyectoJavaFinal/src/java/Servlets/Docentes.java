/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Servlets;

import Modelos.Curso;
import Modelos.Docente;
import Modelos.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author usuario
 */
@WebServlet(name = "Docentes", urlPatterns = {"/Docentes"})
public class Docentes extends HttpServlet {

        private void saveDocente(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            PrintWriter out = response.getWriter();
            HttpSession sh = request.getSession();
            Docente doc;
            
            if(sh.getAttribute("docenteActualizar")!=null){
                doc = (Docente) sh.getAttribute("docenteActualizar");
                doc.setDocumento(request.getParameter("Documento"));
                doc.setNombre(request.getParameter("Nombre"));
                doc.setApellido(request.getParameter("Apellido"));
                doc.setEmail(request.getParameter("Email"));
                doc.setProfesion(request.getParameter("Profesion"));
                doc.setDescripcionPerfil(request.getParameter("DescripcionPerfil"));
            }else
                doc = new Docente(request.getParameter("Documento"), request.getParameter("Nombre"), request.getParameter("Apellido"), request.getParameter("Email"),request.getParameter("Profesion"),request.getParameter("DescripcionPerfil"));
            
            Session s = Modelos.HibernateUtil.getSessionFactory().openSession();
            Transaction tx = s.beginTransaction();
            s.saveOrUpdate(doc);
            //Obtenemos la session del cliente
            sh.setAttribute("docente", doc);
            tx.commit();
            s.close();
            response.sendRedirect("Docentes?c=select");
    }
         
        private void SelectDocente(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        Session s = HibernateUtil.getSessionFactory().openSession();
        Query q = s.createQuery("SELECT l FROM Docente l");
        ArrayList<Docente> docente =  (ArrayList) q.list();
        
        request.setAttribute("docente", docente);
        s.close();
        request.getRequestDispatcher("Docentes?c=select").forward(request, response);
    }
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession sh = request.getSession();
        if(sh.getAttribute("usuario")==null){
            response.sendRedirect("Login.jsp");
        }else{
            if(request.getParameter("c").equalsIgnoreCase("save")){
                saveDocente(request,response);
            }if(request.getParameter("c").equalsIgnoreCase("select")){
                SelectDocente(request,response);
            }

            else {
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
