/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Servlets;

import Modelos.HibernateUtil;
import Modelos.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author ASPIRE
 */
@WebServlet(name = "Usuarios", urlPatterns = {"/Usuarios"})
public class Usuarios extends HttpServlet {

    
    private void saveUsuarios(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            PrintWriter out = response.getWriter();
            HttpSession sh = request.getSession();
            Usuario l;
            
             if(sh.getAttribute("UsuarioActualizar")!=null){
                l = (Usuario) sh.getAttribute("UsuarioActualizar");
                l.setDocumento(request.getParameter("Documento"));
                l.setNombres(request.getParameter("Nombres"));
                l.setApellidos(request.getParameter("Apellidos"));
                l.setDireccion(request.getParameter("Direccion"));
                l.setTelefono(request.getParameter("Telefono"));
                l.setEmail(request.getParameter("Email"));
                l.setContrasena(request.getParameter("Contrasena"));
                l.setTipoUsuario(request.getParameter("TipoUsuario"));
                
            }else
            
            
            l = new Usuario(request.getParameter("Documento"), request.getParameter("Nombres"),request.getParameter("Apellidos"),request.getParameter("Direccion"),request.getParameter("Telefono"),request.getParameter("Email"),request.getParameter("Contrasena"),request.getParameter("TipoUsuario"));
            
            Session s = Modelos.HibernateUtil.getSessionFactory().openSession();
            Transaction tx = s.beginTransaction();
            s.saveOrUpdate(l);
            //Obtenemos la session del cliente
            
            sh.setAttribute("Usuario", l);
            tx.commit();
            s.close();
            response.sendRedirect("Login.jsp");
    }
    
    private void adminUsuarios(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        Session s = HibernateUtil.getSessionFactory().openSession();
        Query q = s.createQuery("SELECT l FROM Usuario l");
        ArrayList<Usuario> usuarios =  (ArrayList) q.list();
       
        request.setAttribute("usuario", usuarios);
        s.close();
        request.getRequestDispatcher("VerUsuarios.jsp").forward(request, response);
     }
     
     
      private void UpdateUsuarios(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        Session s = HibernateUtil.getSessionFactory().openSession();
        Query q = s.createQuery("SELECT l FROM Usuario l WHERE IsUsuario=?");
        q.setInteger(0, Integer.parseInt(request.getParameter("i")));
        
        Usuario usuarios = (Usuario) q.uniqueResult();
        s.close();
        
        HttpSession sh = request.getSession();
        sh.setAttribute("UsuarioActualizar", usuarios);
        
        request.setAttribute("usuarios", usuarios);
        request.getRequestDispatcher("Usuario.jsp").forward(request, response);
        
    }
    
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        if(request.getParameter("a").equalsIgnoreCase("saveUsuarios")){
            saveUsuarios(request,response);
        }
        
        else if(request.getParameter("a").equalsIgnoreCase("admin")){
            adminUsuarios(request,response);
        }
        
         else if(request.getParameter("a").equalsIgnoreCase("update")){
            UpdateUsuarios(request,response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
