/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Servlets;

import Modelos.Curso;
import Modelos.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author ASPIRE
 */
@WebServlet(name="Cursos", urlPatterns={"/Cursos"})
public class Cursos extends HttpServlet {
   
    private void saveCurso(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            PrintWriter out = response.getWriter();
            HttpSession sh = request.getSession();
            Curso cur; 
            
            if(sh.getAttribute("cursoActualizar")!=null){
                cur = (Curso) sh.getAttribute("cursoActualizar");
                cur.setCodigo(request.getParameter("codigo"));
                cur.setNombre(request.getParameter("nombre"));
                cur.setValor(Integer.parseInt(request.getParameter("valor")));
                cur.setDescripcion(request.getParameter("descripcion"));
            }else
                cur = new Curso(request.getParameter("codigo"), request.getParameter("nombre"), Integer.parseInt(request.getParameter("valor")), request.getParameter("descripcion"));
            
            Session s = Modelos.HibernateUtil.getSessionFactory().openSession();
            Transaction tx = s.beginTransaction();
            s.saveOrUpdate(cur);
            //Obtenemos la session del cliente
            
            sh.setAttribute("curso", cur);
            tx.commit();
            s.close();
            response.sendRedirect("Cursos?c=select");
    }
    
    private void SelectCurso(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        Session s = HibernateUtil.getSessionFactory().openSession();
        Query q = s.createQuery("SELECT l FROM Curso l");
        ArrayList<Curso> curso =  (ArrayList) q.list();
        
        request.setAttribute("cursos", curso);
        s.close();
        request.getRequestDispatcher("VerCursos.jsp").forward(request, response);
    }
    
    private void UpdateCurso(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        Session s = HibernateUtil.getSessionFactory().openSession();
        Query q = s.createQuery("SELECT c FROM Curso c WHERE IdCurso=?");
        q.setInteger(0, Integer.parseInt(request.getParameter("i")));
        
        Curso curso = (Curso) q.uniqueResult();
        s.close();
        
        HttpSession sh = request.getSession();
        sh.setAttribute("ActualizarCurso", curso);
        
        request.setAttribute("curso", curso);
        request.getRequestDispatcher("Curso.jsp").forward(request, response);
        
    }
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
     throws ServletException, IOException {
        HttpSession sh = request.getSession();
        if(sh.getAttribute("usuario") == null){
            response.sendRedirect("Login.jsp");
        }else{
            if(request.getParameter("c").equalsIgnoreCase("save")){
                saveCurso(request,response);
            }if(request.getParameter("c").equalsIgnoreCase("select")){
                SelectCurso(request,response);
            }
        }
    } 
        
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
