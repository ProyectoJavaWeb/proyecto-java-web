/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Servlets;

import Modelos.Docente;
import Modelos.Usuario;
import Modelos.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author ASPIRE
 */
@WebServlet(name="ServletLogin", urlPatterns={"/ServletLogin"})
public class ServletLogin extends HttpServlet {
    
    
    private void Ingresar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpSession s = request.getSession(); 
        Session sesion = HibernateUtil.getSessionFactory().openSession();
                
        String usu, pass;
        usu = request.getParameter("Usuario");
        pass = request.getParameter("Contrasena");
        Query q = sesion.createQuery("SELECT u FROM Docente u WHERE u.Email=? AND u.Documento=?");
        Query q2 = sesion.createQuery("SELECT u FROM Usuario u WHERE u.Email=? AND u.Contrasena=?");
        q.setString(0, usu);
        q.setString(1, pass);
        q2.setString(0, usu);
        q2.setString(1, pass);
        
        Docente doc = (Docente) q.uniqueResult();
        Usuario user = (Usuario) q2.uniqueResult();
        
        //deberíamos buscar el usuario en la base de datos, pero dado que se escapa de este tema, ponemos un ejemplo en el mismo código
        if( (doc!=null)){
            //si coincide usuario y password y además no hay sesión iniciada
            s.setAttribute("usuario", usu);
            //redirijo a página con información de login exitoso
            response.sendRedirect("IndexDocentes.jsp");
            
        }else if( (user!=null)){
            s.setAttribute("usuario", usu);
            response.sendRedirect("IndexEstudiante.jsp");
        }else{
            response.sendRedirect("Login.jsp");
        }
    }
    
    private void Salir(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        String action=(request.getPathInfo()!=null?request.getPathInfo():"");
        HttpSession sesion = request.getSession();
        //if(action.equals("/out")){
        //sesion.removeAttribute("usuario");
            sesion.invalidate();
            response.sendRedirect("index.html");
       // }

    }
    
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        if(request.getParameter("l").equalsIgnoreCase("ingresar")){
            Ingresar(request,response);
        }if(request.getParameter("l").equalsIgnoreCase("salir")){
            Salir(request,response);
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
