/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Modelos.Curso;
import Modelos.Docente;
import Modelos.Inscripcion;
import Modelos.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author PC 17
 */
@WebServlet(name = "Inscripciones", urlPatterns = {"/Inscripciones"})
public class Inscripciones extends HttpServlet {
    
    private void saveInscripcion(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            PrintWriter out = response.getWriter();
            HttpSession sh = request.getSession();
            Session sesion = Modelos.HibernateUtil.getSessionFactory().openSession();
            //Inscripcion ins; 
            String fecha, estado;
            String idusu, idcurso;
            
            fecha = (request.getParameter("fecha"));
            estado =(request.getParameter("estado"));
            idusu = (request.getParameter("curso"));
            idcurso =(request.getParameter("usuario"));
            
            Query us = sesion.createQuery("SELECT u FROM Usuario u WHERE u.IdUsuario=?");
            Query cu = sesion.createQuery("SELECT c FROM Curso c WHERE c.IdCurso=?");
            us.setString(0, idusu);
           
            cu.setString(0, idcurso);
            
            

            Usuario usua = (Usuario) us.uniqueResult();
            Curso curs = (Curso) cu.uniqueResult();
            
            Inscripcion ins = new Inscripcion(fecha, estado, curs, usua);
            
            Session s = Modelos.HibernateUtil.getSessionFactory().openSession();
            Transaction tx = s.beginTransaction();
            s.save(ins);
            //Obtenemos la session del cliente
            
            sh.setAttribute("inscripcion", ins);
            tx.commit();
            s.close();
            response.sendRedirect("Inscripciones?i=select");
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
