/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;



import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Modelos.HibernateUtil;
import Modelos.Pago;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author JAVIERV
 */

@WebServlet(name="ServletPago", urlPatterns={"/ServletPago"})
public class Pagos extends HttpServlet {


//private class ServletPago extends HttpServlet{
    
    private void savePago(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
            PrintWriter out = response.getWriter();
            HttpSession session = request.getSession();
            Pago pag;
            
            if(session.getAttribute("Actualizarpago")!=null){
                pag = (Pago) session.getAttribute("ActualizarPago");
                pag.setFechaPago(request.getParameter("fechapago"));
                pag.setValor(Integer.parseInt(request.getParameter("valor")));
               
           
            }else
                pag = new Pago(request.getParameter("FechaPago"),Integer.parseInt(request.getParameter("Valor")));
            
               Session s = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = s.beginTransaction();
            s.saveOrUpdate(pag);
            
              session.setAttribute("pagos", pag);
            tx.commit();
            s.close();
            response.sendRedirect("ServletPago?c=select");
           
            
    }
    

    private void SelectPago(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        Session s = HibernateUtil.getSessionFactory().openSession();
        Query q = s.createQuery("SELECT l FROM Pago l");
        ArrayList<Pago> pago =  (ArrayList) q.list();
        
        request.setAttribute("pagos", pago);
        s.close();
        request.getRequestDispatcher("Pagos.jsp").forward(request, response);
    }
    
//    private void UpdatePago(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        PrintWriter out = response.getWriter();
//        
//        Session s = NewHibernateUtil1.getSessionFactory().openSession();
//        Query q = s.createQuery("SELECT c FROM  c WHERE id_pago=?");
//        q.setInteger(0, Integer.parseInt(request.getParameter("i")));
//        
//        Pago pago = (Pago) q.uniqueResult();
//        s.close();
//        
//        HttpSession sh = request.getSession();
//        sh.setAttribute("ActualizarPago",pago);
//        
//        request.setAttribute("pagos",pago );
//        request.getRequestDispatcher("Pagos.jsp").forward(request, response);
//        
//    }





//@WebServlet(name = "ServletPago", urlPatterns = {"/ServletPago"})
//public class ServletPago extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession sh = request.getSession();
        if(sh.getAttribute("usuario")==null){
            response.sendRedirect("Login.jsp");
        }else{  
            
            if(request.getParameter("c").equalsIgnoreCase("save")){
                savePago(request,response);
            }if(request.getParameter("c").equalsIgnoreCase("select")){
                 SelectPago(request,response);
            }
        }       
    }

//    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       processRequest(request, response);
    }

//    /**
//     * Handles the HTTP <code>POST</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

//    /**
//     * Returns a short description of the servlet.
//     *
//     * @return a String containing servlet description
//     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}


