/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author PC 17
 */
public class Curso implements Serializable {
    
    private int IdCurso;
    private String Codigo;
    private String Nombre;
    private int Valor;
    private String Descripcion;
    private Set<Docente> DocenteCurso;
    private List<Inscripcion> Inscripcion = new ArrayList<Inscripcion>();    

    public Curso(){}

    public Curso(String Codigo, String Nombre, int Valor, String Descripcion) {
        this.Codigo = Codigo;
        this.Nombre = Nombre;
        this.Valor = Valor;
        this.Descripcion = Descripcion;
    }
    
     public static ArrayList<Curso> getcurso(){
       
        ArrayList<Curso> ci = new ArrayList<Curso>();
        try {

        Session s = HibernateUtil.getSessionFactory().openSession();
        Query q = s.createQuery("SELECT C FROM Curso C ");
        ci = (ArrayList) q.list();
  
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }        
        return ci;    
    }
     
        private Session sesion;
        private Transaction tx;
     
       private void inicia() throws HibernateException{
       sesion= HibernateUtil.getSessionFactory().openSession();
       tx= sesion.beginTransaction();       
   }
   
   public Curso cursos (int IdCurso) throws  HibernateException{
       Curso cur;
       
       try{
       inicia();
       Query q = sesion.createQuery("SELECT c FROM Curso c WHERE IdCurso=?");
       q.setInteger(0,IdCurso);
       cur =(Curso) q.uniqueResult();

       }finally{
         sesion.close();
       }
       return cur;       
   } 
        
    /**
     * @return the IdCurso
     */
    public int getIdCurso() {
        return IdCurso;
    }

    /**
     * @param IdCurso the IdCurso to set
     */
    public void setIdCurso(int IdCurso) {
        this.IdCurso = IdCurso;
    }

    /**
     * @return the Codigo
     */
    public String getCodigo() {
        return Codigo;
    }

    /**
     * @param Codigo the Codigo to set
     */
    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    /**
     * @return the Nombre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * @param Nombre the Nombre to set
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    /**
     * @return the Descripcion
     */
    public String getDescripcion() {
        return Descripcion;
    }

    /**
     * @param Descripcion the Descripcion to set
     */
    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    /**
     * @return the Valor
     */
    public int getValor() {
        return Valor;
    }

    /**
     * @param Valor the Valor to set
     */
    public void setValor(int Valor) {
        this.Valor = Valor;
    }

    /**
     * @return the DocenteCurso
     */
    public Set<Docente> getDocenteCurso() {
        return DocenteCurso;
    }

    /**
     * @param DocenteCurso the DocenteCurso to set
     */
    public void setDocenteCurso(Set<Docente> DocenteCurso) {
        this.DocenteCurso = DocenteCurso;
    }

    /**
     * @return the Inscripcion
     */
    public List<Inscripcion> getInscripcion() {
        return Inscripcion;
    }

    /**
     * @param Inscripcion the Inscripcion to set
     */
    public void setInscripcion(List<Inscripcion> Inscripcion) {
        this.Inscripcion = Inscripcion;
    }

   
}
