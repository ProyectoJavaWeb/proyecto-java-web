/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelos;

import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author ASPIRE
 */
public class DocenteCurso {
    
    private int IdDocenteCurso;
    private String Materia;
    private Curso Curso;
    private Docente Docente;

    public DocenteCurso(){
    }
    
    public DocenteCurso(String Materia, Curso Curso, Docente Docente) {
        this.Materia = Materia;
        this.Curso = Curso;
        this.Docente = Docente;
    }

    /**
     * @return the IdDocenteCurso
     */
    public int getIdDocenteCurso() {
        return IdDocenteCurso;
    }

    /**
     * @param IdDocenteCurso the IdDocenteCurso to set
     */
    public void setIdDocenteCurso(int IdDocenteCurso) {
        this.IdDocenteCurso = IdDocenteCurso;
    }

    /**
     * @return the Materia
     */
    public String getMateria() {
        return Materia;
    }

    /**
     * @param Materia the Materia to set
     */
    public void setMateria(String Materia) {
        this.Materia = Materia;
    }

    /**
     * @return the Curso
     */
    public Curso getCurso() {
        return Curso;
    }

    /**
     * @param Curso the Curso to set
     */
    public void setCurso(Curso Curso) {
        this.Curso = Curso;
    }

    /**
     * @return the Docente
     */
    public Docente getDocente() {
        return Docente;
    }

    /**
     * @param Docente the Docente to set
     */
    public void setDocente(Docente Docente) {
        this.Docente = Docente;
    }
    
    
   
   
    
}
