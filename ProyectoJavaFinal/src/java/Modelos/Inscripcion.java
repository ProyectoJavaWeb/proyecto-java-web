/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PC 17
 */
public class Inscripcion {
    
    private int IdInscripcion;
    private String Fecha;
    private String Estado;
    private Curso Curso;
    private Usuario Usuario;
    private Pago Pago;
    
    
    public Inscripcion(){}

    public Inscripcion( String Fecha, String Estado, Curso Curso, Usuario Usuario) {
        
        this.Fecha = Fecha;
        this.Estado = Estado;
        this.Curso = Curso;
        this.Usuario = Usuario;
        //this.Pago = Pago;
    }
    

    
    
    /**
     * @return the IdInscripcion
     */
    public int getIdInscripcion() {
        return IdInscripcion;
    }

    /**
     * @param IdInscripcion the IdInscripcion to set
     */
    public void setIdInscripcion(int IdInscripcion) {
        this.IdInscripcion = IdInscripcion;
    }

    /**
     * @return the Fecha
     */
    public String getFecha() {
        return Fecha;
    }

    /**
     * @param Fecha the Fecha to set
     */
    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    /**
     * @return the Estado
     */
    public String getEstado() {
        return Estado;
    }

    /**
     * @param Estado the Estado to set
     */
    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    /**
     * @return the Curso
     */
    public Curso getCurso() {
        return Curso;
    }

    /**
     * @param Curso the Curso to set
     */
    public void setCurso(Curso Curso) {
        this.Curso = Curso;
    }

    /**
     * @return the Usuario
     */
    public Usuario getUsuario() {
        return Usuario;
    }

    /**
     * @param Usuario the Usuario to set
     */
    public void setUsuario(Usuario Usuario) {
        this.Usuario = Usuario;
    }

    
    /**
     * @return the Pago
     */
    public Pago getPago() {
        return Pago;
    }

    /**
     * @param Pago the Pago to set
     */
    public void setPago(Pago Pago) {
        this.Pago = Pago;
    }
    
}
