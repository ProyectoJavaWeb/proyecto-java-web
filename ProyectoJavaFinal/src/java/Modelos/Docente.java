/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author usuario
 */
public class  Docente implements Serializable {
    private int IdDocente;
    private String Documento;
    private String Nombre;
    private String Apellido;
    private String Email;
    private String Profesion;
    private String descripcionPerfil;
    private Set<DocenteCurso> DocenteCurso;
    
    
    public Docente(){
    }

    public Docente(String Documento, String Nombre, String Apellido, String Email, String Profesion, String descripcionPerfil) {
        this.Documento = Documento;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.Email = Email;
        this.Profesion = Profesion;
        this.descripcionPerfil = descripcionPerfil;
    }

    
     public static ArrayList<Docente> getdocente(){
       
        ArrayList<Docente> ci = new ArrayList<Docente>();
        try {

        Session s = HibernateUtil.getSessionFactory().openSession();
        Query q = s.createQuery("SELECT C FROM Docente C ");
        ci = (ArrayList) q.list();
  
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }        
        return ci;    
    } 
     
         private Session sesion;
         private Transaction tx;
     
          private void inicia() throws HibernateException{
          sesion= HibernateUtil.getSessionFactory().openSession();
          tx= sesion.beginTransaction();       
          }
   
       public Docente docentes (int IdDocente) throws  HibernateException{
       Docente doc;       
       try{
       inicia();
       Query q = sesion.createQuery("SELECT c FROM Docente c WHERE IdDocente=?");
       q.setInteger(0,IdDocente);
       doc =(Docente) q.uniqueResult();    
       }finally{
         sesion.close();
       }
       return doc;
   }   
     
     /**
     * @return the IdDocente
     */
    public int getIdDocente() {
        return IdDocente;
    }

    /**
     * @param IdDocente the IdDocente to set
     */
    public void setIdDocente(int IdDocente) {
        this.IdDocente = IdDocente;
    }
   

    public String getDocumento() {
        return Documento;
    }

    public void setDocumento(String Documento) {
        this.Documento = Documento;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getProfesion() {
        return Profesion;
    }

    public void setProfesion(String Profesion) {
        this.Profesion = Profesion;
    }

    public String getDescripcionPerfil() {
        return descripcionPerfil;
    }

    public void setDescripcionPerfil(String descripcionPerfil) {
        this.descripcionPerfil = descripcionPerfil;
    }

    /**
     * @return the DocenteCurso
     */
    public Set<DocenteCurso> getDocenteCurso() {
        return DocenteCurso;
    }

    /**
     * @param DocenteCurso the DocenteCurso to set
     */
    public void setDocenteCurso(Set<DocenteCurso> DocenteCurso) {
        this.DocenteCurso = DocenteCurso;
    }

    
}
