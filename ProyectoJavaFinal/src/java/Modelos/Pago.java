/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PC 17
 */
public class Pago {
    
    private int IdPago;
    private String FechaPago;
    private int Valor;
    private List<Inscripcion> Inscripcion = new ArrayList<Inscripcion>();

    public Pago(){}
    
    public Pago( String FechaPago, int Valor) {
        
        this.FechaPago = FechaPago;
        this.Valor = Valor;
    }

    
    
    
    /**
     * @return the IdPago
     */
    public int getIdPago() {
        return IdPago;
    }

    /**
     * @param IdPago the IdPago to set
     */
    public void setIdPago(int IdPago) {
        this.IdPago = IdPago;
    }

    /**
     * @return the FechaPago
     */
    public String getFechaPago() {
        return FechaPago;
    }

    /**
     * @param FechaPago the FechaPago to set
     */
    public void setFechaPago(String FechaPago) {
        this.FechaPago = FechaPago;
    }

    /**
     * @return the Valor
     */
    public int getValor() {
        return Valor;
    }

    /**
     * @param Valor the Valor to set
     */
    public void setValor(int Valor) {
        this.Valor = Valor;
    }

    /**
     * @return the Inscripcion
     */
    public List<Inscripcion> getInscripcion() {
        return Inscripcion;
    }

    /**
     * @param Inscripcion the Inscripcion to set
     */
    public void setInscripcion(List<Inscripcion> Inscripcion) {
        this.Inscripcion = Inscripcion;
    }
}
